package fr.ul.iutmetz.info.wce.devisev0;

import android.content.Context;
import android.widget.Toast;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Celeste on 08/12/2017.
 */

public class SerialisationPortefeuille {

    private static Context context;
    //Singleton
    private static SerialisationPortefeuille instance;
    private SerialisationPortefeuille() {}
    public static SerialisationPortefeuille getInstance(Context contextRecu) {
        if (instance==null) {
            instance = new SerialisationPortefeuille();
            context = contextRecu;
        }
        return instance;
    }

    public Portefeuille charge(){
        ObjectInputStream ois = null;
        Portefeuille portefeuille;
        try {
            ois = new ObjectInputStream(this.context.openFileInput("portefeuille.ser"));
            portefeuille = (Portefeuille) ois.readObject();
        }catch (IOException | ClassNotFoundException e){
            portefeuille = new Portefeuille();
        }finally {
            try{
                if (ois != null){
                    ois.close();
                }
            }catch (Exception e){
                Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
            }
        }
        return portefeuille;
    }

    public void ecriture(Portefeuille portefeuille){
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(this.context.openFileOutput("portefeuille.ser",Context.MODE_PRIVATE));
            oos.writeObject(portefeuille);
        }catch (IOException  e){
           System.out.print("rien écrit");
        }finally {
            try{
                if (oos != null){
                    oos.close();
                }
            }catch (Exception e){
                Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
            }
        }

    }

}
