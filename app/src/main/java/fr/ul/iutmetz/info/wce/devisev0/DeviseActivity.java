package fr.ul.iutmetz.info.wce.devisev0;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class DeviseActivity extends AppCompatActivity implements DialogInterface.OnClickListener{
    private EditText etMontant;
    private TextView txtEnPoche;
    private Devise d;
    public static final int OK=1;
    public static final int CANCEL=2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.devise_activity);
        if(savedInstanceState == null){
            d = (Devise) this.getIntent().getExtras().get("devise");
        }else{
            this.d = (Devise)savedInstanceState.getSerializable("devise");
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
        this.etMontant = (EditText) this.findViewById(R.id.ma_montant);
        this.txtEnPoche = (TextView) this.findViewById(R.id.ma_enpoche);
        txtEnPoche.setText(String.format(getString(R.string.ma_enpoche),this.d.getNom(),this.d.getMontant()));
    }



    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putSerializable("devise",d);
    }

    public void retrait(View view){
        try{
            float montant = Float.parseFloat(this.etMontant.getText().toString());
            this.d.retrait(montant);
            etMontant.setText("");
            Toast.makeText(DeviseActivity.this," - " + montant,Toast.LENGTH_SHORT).show();
            txtEnPoche.setText(String.format(getString(R.string.ma_enpoche),this.d.getNom(),this.d.getMontant()));
            if (d.getMontant() == 0){
                SuppressionAlerte alerte = new SuppressionAlerte();
                alerte.show(getFragmentManager(),"suppression");
            }else{
                Intent donnees  = new Intent();
                donnees.putExtra("devise",d);
                this.setResult(OK,donnees);
                this.finish();
            }

        }catch (IllegalArgumentException iae){
            Toast.makeText(DeviseActivity.this,iae.getMessage() ,Toast.LENGTH_LONG).show();
        }
    }

    public void ajouter(View view){
         try{
             float montant = Float.parseFloat(this.etMontant.getText().toString());
             this.d.ajout(montant);
             etMontant.setText("");
             Toast.makeText(DeviseActivity.this," + " + montant,Toast.LENGTH_SHORT).show();
             txtEnPoche.setText(String.format(getString(R.string.ma_enpoche),this.d.getNom(),this.d.getMontant()));
             Intent donnees  = new Intent();
             donnees.putExtra("devise",d);
             this.setResult(OK,donnees);
             this.finish();
         }catch (IllegalArgumentException iae){
             Toast.makeText(DeviseActivity.this,iae.getMessage(),Toast.LENGTH_LONG).show();
         }

    }

    public void retour(View v){
        this.setResult(CANCEL);
        this.finish();
    }

    @Override
    public void onBackPressed(){
        this.retour(null);
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int which) {
         if(which == DialogInterface.BUTTON_POSITIVE){
             Intent eliminar  = new Intent();
             eliminar.putExtra("retirer",d);
             this.setResult(OK,eliminar);
             this.finish();
         }else if(which == DialogInterface.BUTTON_NEGATIVE){
             Intent donnees  = new Intent();
             donnees.putExtra("devise",d);
             this.setResult(OK,donnees);
             this.finish();
         }
    }
}
