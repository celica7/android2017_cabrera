package fr.ul.iutmetz.info.wce.devisev0;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by Celeste on 21/12/2017.
 */

public class NomDeviseAlerte extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState){
        Activity activite = getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(activite);
        builder.setMessage(R.string.da_message_nomDevise).setTitle(R.string.da_modifier_nomDevise);
        DialogInterface.OnClickListener ecouter = (DialogInterface.OnClickListener) activite;
        builder.setPositiveButton(R.string.da_ok,ecouter);
        return builder.create();
    }

}
