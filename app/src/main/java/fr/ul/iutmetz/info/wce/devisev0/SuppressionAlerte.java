package fr.ul.iutmetz.info.wce.devisev0;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by Celeste on 20/12/2017.
 */

public class SuppressionAlerte extends DialogFragment{
    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState){
        Activity activite = getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(activite);
        builder.setMessage(R.string.da_confirme_suppression).setTitle(R.string.da_devise_nulle);
        DialogInterface.OnClickListener ecouter = (DialogInterface.OnClickListener) activite;
        builder.setPositiveButton(R.string.da_oui,ecouter);
        builder.setNegativeButton(R.string.da_non,ecouter);
        return builder.create();
    }

}
