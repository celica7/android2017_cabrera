package fr.ul.iutmetz.info.wce.devisev0;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MajNomDeviseActivity extends AppCompatActivity implements DialogInterface.OnClickListener{
    private String nomDevise="";
    private String errorNom="";
    private ArrayList liste = null;
    private EditText etNom;
    private TextView tvNom;
    public static final int OK=1;
    public static final int CANCEL=2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maj_nom_devise);
        try {
            nomDevise = this.getIntent().getExtras().getString("nom");
            liste = this.getIntent().getExtras().getParcelableArrayList("lista");
        } catch (NullPointerException npe) {
            System.out.print("appelle vide");
        }

    }

    @Override
    protected void onStart(){
        super.onStart();
        this.etNom = (EditText) this.findViewById(R.id.nomDevise);
        this.tvNom = (TextView) this.findViewById(R.id.nom);
        if(this.nomDevise == null){
            this.tvNom.setText(getString(R.string.nomDevise));
        }else{
            this.tvNom.setText(getString(R.string.da_modification));
            this.etNom.setText(nomDevise);
        }
    }

    public void ok(View v){
         Intent donnees  = new Intent();
         nomDevise = this.etNom.getText().toString().trim();
         errorNom = getString(R.string.da_errorNom);
         if(nomDevise.length() == 0 ){
             Toast.makeText(MajNomDeviseActivity.this,errorNom,Toast.LENGTH_LONG).show();
         }else if(liste == null){
             donnees.putExtra("nom",this.etNom.getText().toString().trim());
             this.setResult(OK,donnees);
             this.finish();
         }else if(liste != null && liste.contains(new Devise(nomDevise))){
             NomDeviseAlerte alerte = new NomDeviseAlerte();
             alerte.show(getFragmentManager(),"modifier");
         } else {
             donnees.putExtra("nom",this.etNom.getText().toString().trim());
             this.setResult(OK,donnees);
             this.finish();
         }
    }

    public void annuler(View v){
        this.setResult(CANCEL);
        this.finish();
    }

    @Override
    public void onBackPressed(){
        this.annuler(null);
    }


    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        this.etNom.setText("");

    }
}
