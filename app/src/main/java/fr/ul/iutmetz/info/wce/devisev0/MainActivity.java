package fr.ul.iutmetz.info.wce.devisev0;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Surface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    private ListView lstDevises;
    private GridLayout grdDevises;
    private TextView txtPortefeuille;
    private Portefeuille monPortefeuille;
    private int indexListe;
    private static final int GESTION_DEVISE = 0;
    private static final int CREATION_DEVISE = 1;
    private static final int MODIFICATION_NOM_DEVISE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(savedInstanceState == null){
            this.monPortefeuille = SerialisationPortefeuille.getInstance(this).charge();

        }else{
            this.monPortefeuille = (Portefeuille) savedInstanceState.getSerializable("portefeuille");
        }

    }

    @Override
    protected void onStart(){
      super.onStart();
       if (this.getWindowManager().getDefaultDisplay().getRotation() == Surface.ROTATION_90 ||
               this.getWindowManager().getDefaultDisplay().getRotation() == Surface.ROTATION_270
               ){
           this.lstDevises = this.findViewById(R.id.ma_liste);
           ArrayAdapter<Devise> adaptateur = new ArrayAdapter<Devise>(this,android.R.layout.simple_list_item_1,this.monPortefeuille.getValeurs());
           this.lstDevises.setAdapter(adaptateur);
           this.lstDevises.setOnItemClickListener(this);
           this.lstDevises.setOnItemLongClickListener(this);
       }else{
           this.grdDevises = this.findViewById(R.id.ma_grille);
           this.txtPortefeuille = this.findViewById(R.id.ma_portefeuille);
           this.grdDevises.removeAllViews();
           this.grdDevises.setColumnCount(3);
           for (int i=0; i<this.monPortefeuille.getValeurs().size(); i++){
               Devise d = monPortefeuille.getValeurs().get(i);
               final Button btnDevise = new Button(this);
               btnDevise.setText(d.getNom());
               btnDevise.setId(i);
               /*Méthod pour gérer devise*/
               btnDevise.setOnClickListener(new View.OnClickListener() {
                   public void onClick(View v) {
                       indexListe = btnDevise.getId();
                       Devise d = (Devise) monPortefeuille.getValeurs().get( btnDevise.getId());
                       Intent appelActDevise = new Intent(MainActivity.this,DeviseActivity.class);
                       appelActDevise.putExtra("devise", d);
                       startActivityForResult(appelActDevise,GESTION_DEVISE);
                   }
               });
               /*Méthod pour modifier le nom de la devise*/
               btnDevise.setOnLongClickListener(new View.OnLongClickListener() {
                   public boolean onLongClick(View view) {
                       indexListe = btnDevise.getId();
                       Devise d = (Devise) monPortefeuille.getValeurs().get( btnDevise.getId());
                       Intent appelActivite = new Intent(MainActivity.this,MajNomDeviseActivity.class);
                       appelActivite.putExtra("nom", d.getNom());
                       appelActivite.putExtra("lista",monPortefeuille.getValeurs());
                       startActivityForResult(appelActivite,MODIFICATION_NOM_DEVISE);
                       return true;
                   }
               });

               btnDevise.setTextColor(Color.WHITE);
               btnDevise.getBackground().setColorFilter(
                       ContextCompat.getColor(this.getApplicationContext(), R.color.colorAccent),
                       PorterDuff.Mode.MULTIPLY);

                this.grdDevises.addView(btnDevise);

           }
           /*Afficher portefeuille*/
           this.txtPortefeuille.setText(getString(R.string.da_contenuDuPortefeuille) +"\n" + monPortefeuille.toString());
       }
    }

    @Override
    protected void onPause(){
        super.onPause();
        SerialisationPortefeuille.getInstance(this).ecriture(monPortefeuille);
    }

    @Override
    protected void onStop(){
        super.onStop();
        SerialisationPortefeuille.getInstance(this).ecriture(monPortefeuille);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putSerializable("portefeuille",monPortefeuille);
    }

    public void creerNouvelleDevise(View v){
        Intent appelActivite = new Intent(this,MajNomDeviseActivity.class);
        appelActivite.putExtra("lista",monPortefeuille.getValeurs());
        startActivityForResult(appelActivite,CREATION_DEVISE);
    }

    public void retirerDevise(){

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        this.indexListe = position;
        Devise d = (Devise) this.lstDevises.getItemAtPosition(this.indexListe);
        Intent appelActDevise = new Intent(this,DeviseActivity.class);
        appelActDevise.putExtra("devise", d);
        startActivityForResult(appelActDevise,GESTION_DEVISE);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        this.indexListe = position;
        Devise d = (Devise) this.lstDevises.getItemAtPosition(this.indexListe);
        Intent appelActivite = new Intent(this,MajNomDeviseActivity.class);
        appelActivite.putExtra("nom", d.getNom());
        appelActivite.putExtra("lista",monPortefeuille.getValeurs());
        startActivityForResult(appelActivite,MODIFICATION_NOM_DEVISE);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == MajNomDeviseActivity.OK || resultCode==DeviseActivity.OK){
             if (requestCode == CREATION_DEVISE){
                 Devise devise = new Devise();
                 devise.setNom(data.getExtras().getString("nom"));
                 monPortefeuille.ajout(devise);
             }else if(requestCode == MODIFICATION_NOM_DEVISE){
                 monPortefeuille.getValeurs().get(this.indexListe).setNom(data.getExtras().getString("nom"));
             }else if(requestCode == GESTION_DEVISE){
                 if (data.hasExtra("retirer")){
                     monPortefeuille.getValeurs().remove(this.indexListe);
                 }else{
                     Devise devise = (Devise)data.getExtras().get("devise");
                     monPortefeuille.getValeurs().get(this.indexListe).setMontant(devise.getMontant());
                 }

             }
        }
    }
}
